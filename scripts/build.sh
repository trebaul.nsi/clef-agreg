#!/bin/bash

cloud-localds seed.img cloud-config.yaml
wget https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img
qemu-img resize focal-server-cloudimg-amd64.img +14G
qemu-system-x86_64 -m 8192 -smp 4 -net nic -net user -hda focal-server-cloudimg-amd64.img -hdb seed.img -nographic
qemu-img convert focal-server-cloudimg-amd64.img focal-server-cloudimg-amd64.raw
VBoxManage convertdd focal-server-cloudimg-amd64.raw focal-server-cloudimg-amd64.vdi --format VDI

